# Running Clarin
### Setting up the environment
If you want to run Clarin in a local environment, you must set target property in docker-compose.yml file to `local`. Otherwise, Docker will try to run the handle server; which is not possible in local environment since http://hdl.handle.net/ only talks to our NLP server.
#### Setting up the network connections
If you are on Linux, you do not need to do something for this part, please proceed with Running the containers section.


Communication from a container to the host can be established with the `172.17.10.1` address. This is the default host address for docker on Linux. However, for MacOS, Windows and WSL this IP address is not valid and `host.docker.internal` domain name must be used.

- Change `172.17.0.1` to `host.docker.internal` 
    - `db.url` in `opt/lindat-dspace/installation/config/dspace.cfg`
    - `lr.utilities.db.url` in `opt/lindat-dspace/installation/config/modules/lr.cfg`

### Running the containers
```bash
docker-compose up --build -d
```
Go to `localhost:8080` in your browser


### Creating an administrator accout 
```bash
docker exec -it clarin bash
./opt/lindat-dspace/installation/bin/dspace create-administrator
```
