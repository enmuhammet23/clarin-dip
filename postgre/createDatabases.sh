psql -U postgres -c "CREATE USER dspace WITH ENCRYPTED PASSWORD 'dspace';" && \
createdb --username postgres db_dip --owner dspace --encoding "UTF-8" --template=template0 && \
createdb --username postgres dp_utils --owner dspace --encoding "UTF-8" --template=template0 && \
psql --username postgres --set=utildir="" dp_utils < utilities.sql

