function DSpaceSetupAutocomplete(b, a) {
    console.log("merfe logu");
    $(function() {
        if (a.authorityName == null) {
            a.authorityName = dspace_makeFieldInput(a.inputName, "_authority")
        }
        var g = $("#" + b)[0];
        var d = g.elements[a.inputName].id;
        var e = null;
        if (g.elements[a.authorityName] != null) {
            e = g.elements[a.authorityName].id
        }
        var h = a.contextPath + "/choices/" + a.metadataField;
        var c = a.collection == null ? -1 : a.collection;
        h += "?collection=" + c;
        var f = $("#" + d);
        f.autocomplete({
            source: function(l, k) {
                var j = $("#" + e);
                if (j.length > 0) {
                    j = j[0];
                    j.value = "";
                    DSpaceUpdateConfidence(document, a.confidenceIndicatorID, "blank")
                }
                if (a.confidenceName != null) {
                    var m = j.form.elements[a.confidenceName];
                    if (m != null) {
                        m.value = ""
                    }
                }
                var i = h;
                if (l && l.term) {
                    i += "&query=" + l.term
                }
                $.get(i, function(o) {
                    var n = [];
                    var p = [];
                    $(o).find("Choice").each(function() {
                        var r = $(this).attr("value") ? $(this).attr("value") : null;
                        var q = $(this).text() ? $(this).text() : r;
                        if (!r) {
                            r = q
                        }
                        if (q != null) {
                            n.push({
                                label: q,
                                value: r
                            });
                            p["label: " + q + ", value: " + r] = $(this).attr("authority") ? $(this).attr("authority") : r
                        }
                    });
                    f.data("authorities", p);
                    k(n)
                })
            },
            select: function(j, l) {
                var i = $("#" + e);
                if (i.length > 0) {
                    i = i[0]
                } else {
                    i = null
                }
                var n = f.data("authorities");
                var k = n["label: " + l.item.label + ", value: " + l.item.value];
                if (i != null) {
                    i.value = k;
                    if (a.confidenceName != null) {
                        var m = i.form.elements[a.confidenceName];
                        if (m != null) {
                            m.value = "accepted"
                        }
                    }
                }
                DSpaceUpdateConfidence(document, a.confidenceIndicatorID, k == null || k == "" ? "blank" : "accepted")
            }
        })
    })
}
function DSpaceChoiceLookup(a, n, e, q, h, l, f, k, g) {
    a += "?field=" + n + "&formID=" + e + "&valueInput=" + q + "&authorityInput=" + h + "&collection=" + f + "&isName=" + k + "&isRepeating=" + g + "&confIndicatorID=" + l;
    var j = k ? dspace_makeFieldInput(q, "_last") : q;
    var c = $("input[name = " + j + "]");
    if (c.length == 0) {
        c = $("textarea[name = " + j + "]")
    }
    var i = 0;
    if (c != null) {
        i = c.offset()
    }
    var b = 600;
    var p = 470;
    var d;
    var m;
    if (window.screenX == null) {
        d = window.screenLeft + i.left - (b / 2);
        m = window.screenTop + i.top - (p / 2)
    } else {
        d = window.screenX + i.left - (b / 2);
        m = window.screenY + i.top - (p / 2)
    }
    if (d < 0) {
        d = 0
    }
    if (m < 0) {
        m = 0
    }
    var o = window.open(a, "ignoreme", "width=" + b + ",height=" + p + ",left=" + d + ",top=" + m + ",toolbar=no,menubar=no,location=no,status=no,resizable");
    if (window.focus) {
        o.focus()
    }
    return false
}
function DSpaceChoicesSetup(b) {
    var a = $("#aspect_general_ChoiceLookupTransformer_list_choicesList :header:first");
    a.data("template", a.html());
    a.html("Loading...");
    DSpaceChoicesLoad(b)
}
function DSpaceChoicesLoad(b) {
    var m = $("*[name = paramField]").val();
    var o = $("*[name = paramValue]").val();
    if (!o) {
        o = ""
    }
    var a = $("*[name = paramStart]").val();
    var i = $("*[name = paramLimit]").val();
    var f = $("*[name = paramFormID]").val();
    var k = $("*[name = paramCollection]").val();
    var j = $("*[name = paramIsName]").val() == "true";
    var g = $("*[name = paramIsRepeating]").val() == "true";
    var c = $("*[name = paramIsClosed]").val() == "true";
    var d = $("*[name = contextPath]").val();
    var e = $("*[name = paramFail]").val();
    var q = $("*[name = paramValueInput]").val();
    var h = "";
    var l = $("*[name = paramNonAuthority]");
    if (l.length > 0) {
        h = l.val()
    }
    if (o.length == 0) {
        var p = $(window.opener.document).find("#" + f);
        if (j) {
            o = makePersonName(p.find("*[name = " + dspace_makeFieldInput(q, "_last") + "]").val(), p.find("*[name = " + dspace_makeFieldInput(q, "_first") + "]").val())
        } else {
            o = p.find("*[name = " + q + "]").val()
        }
        if (g) {
            if (j) {
                p.find("*[name = " + dspace_makeFieldInput(q, "_last") + "]").val("");
                p.find("*[name = " + dspace_makeFieldInput(q, "_first") + "]").val("")
            } else {
                p.find("*[name = " + q + "]").val(null)
            }
        }
    }
    var n = $("#lookup_indicator_id");
    n.show("fast");
    $(window).ajaxError(function(t, u, s, r) {
        window.alert(e + " Exception=" + t);
        if (n != null) {
            n.style.display = "none"
        }
    });
    $.ajax({
        url: d + "/choices/" + m,
        type: "GET",
        data: {
            query: o,
            collection: k,
            start: a,
            limit: i
        },
        error: function() {
            window.alert(e + " HTTP error resonse");
            if (n != null) {
                n.style.display = "none"
            }
        },
        success: function(A) {
            var C = $(A).find("Choices");
            var x = C.attr("error");
            if (x != null && x == "true") {
                window.alert(e + " Server indicates error in response.")
            }
            var t = C.find("Choice");
            var y = 1 * C.attr("start");
            var s = y + t.length;
            var D = C.attr("total");
            var z = C.attr("more");
            $("*[name = more]").attr("disabled", !(z != null && z == "true"));
            $("*[name = paramStart]").val(s);
            var E = $("select[name = chooser]:first");
            E.find("option:not(:last)").remove();
            var w = E.find("option:last");
            var r = -1;
            var H = -1;
            $.each(t, function(I) {
                var K = $(this);
                if (K.attr("value") == o) {
                    r = I
                }
                if (K.attr("selected") != undefined) {
                    H = I
                }
                var J = $('<option value="' + K.attr("value") + '">' + K.text() + "</option>");
                J.data("authority", K.attr("authority"));
                if (w.length > 0) {
                    w.insertBefore(J)
                } else {
                    E.append(J)
                }
            });
            if (!c) {
                E.append(new Option(dspace_formatMessage(h, o),o), null)
            }
            var B = -1;
            if (H >= 0) {
                B = H
            } else {
                if (r >= 0) {
                    B = r
                } else {
                    if (E[0].options.length == 1) {
                        B = 0
                    }
                }
            }
            if (B >= 0) {
                E[0].options[B].defaultSelected = true;
                var v = E[0].options[B];
                if (j) {
                    $("*[name = text1]").val(lastNameOf(v.value));
                    $("*[name = text2]").val(firstNameOf(v.value))
                } else {
                    $("*[name = text1]").val(v.value)
                }
            }
            n.hide("fast");
            var u = s + (c ? 0 : 1);
            var G = (D == 0 ? y : (y + 1));
            var F = $("#aspect_general_ChoiceLookupTransformer_list_choicesList :header:first");
            F.html(dspace_formatMessage(F.data("template"), G, u, Math.max(D, u), o))
        }
    })
}
function DSpaceChoicesSelectOnChange() {
    var d = $("#aspect_general_ChoiceLookupTransformer_div_lookup");
    var a = d.find("*[name = chooser]");
    var c = d.find("*[name = paramIsName]").val() == "true";
    var b = a.val();
    if (c) {
        d.find("*[name = text1]").val(lastNameOf(b));
        d.find("*[name = text2]").val(firstNameOf(b))
    } else {
        d.find("*[name = text1]").val(b)
    }
}
function DSpaceChoicesAcceptOnClick() {
    var h = $("*[name = chooser]");
    var f = $("*[name = paramIsName]").val() == "true";
    var b = $("*[name = paramIsRepeating]").val() == "true";
    var l = $("*[name = paramValueInput]").val();
    var i = $("*[name = paramAuthorityInput]").val();
    var a = $("*[name = paramFormID]").val();
    var g = $("*[name = paramConfIndicatorID]").length = 0 ? null : $("*[name = paramConfIndicatorID]").val();
    if (i.length == 0) {
        i = dspace_makeFieldInput(l, "_authority")
    }
    if (l.length > 0) {
        var k = $(window.opener.document).find("#" + a);
        if (f) {
            k.find("*[name = " + dspace_makeFieldInput(l, "_last") + "]").val($("*[name = text1]").val());
            k.find("*[name = " + dspace_makeFieldInput(l, "_first") + "]").val($("*[name = text2]").val())
        } else {
            k.find("*[name = " + l + "]").val($("*[name = text1]").val())
        }
        if (i.length > 0 && k.find("*[name = " + i + "]").length > 0) {
            var e = "";
            var m = i.lastIndexOf("_authority_");
            if (m < 0) {
                e = i.substring(0, i.length - 10) + "_confidence"
            } else {
                e = i.substring(0, m) + "_confidence_" + i.substring(m + 11)
            }
            var d = null;
            var c = h.find(":selected");
            if (c.length >= 0 && c.data("authority") != null) {
                d = c.data("authority");
                k.find("*[name = " + i + "]").val(d)
            }
            k.find("*[name = " + e + "]").val("accepted");
            DSpaceUpdateConfidence(window.opener.document, g, d == null || d == "" ? "blank" : "accepted")
        }
        if (b) {
            var j = k.find("*[name = submit_" + l + "_add]");
            if (j.length > 0) {
                j.click()
            } else {
                alert('Sanity check: Cannot find button named "submit_' + l + '_add"')
            }
        }
    }
    window.close();
    return false
}
function DSpaceChoicesMoreOnClick() {
    DSpaceChoicesLoad(this.form)
}
function DSpaceChoicesCancelOnClick() {
    window.close();
    return false
}
function makePersonName(a, b) {
    return (b == null || b.length == 0) ? a : a + ", " + b
}
function firstNameOf(b) {
    var a = b.indexOf(",");
    return (a < 0) ? "" : stringTrim(b.substring(a + 1))
}
function lastNameOf(b) {
    var a = b.indexOf(",");
    return stringTrim((a < 0) ? b : b.substring(0, a))
}
function stringTrim(b) {
    var c = 0;
    var a = b.length;
    for (; b.charAt(c) == " " && c < a; ++c) {}
    for (; a > c && b.charAt(a - 1) == " "; --a) {}
    return b.slice(c, a)
}
function dspace_formatMessage() {
    var b = dspace_formatMessage.arguments[0];
    var a;
    for (a = 1; a < arguments.length; ++a) {
        var d = "@" + a + "@";
        if (b.search(d) >= 0) {
            var c = dspace_formatMessage.arguments[a];
            if (c == undefined) {
                c = ""
            }
            b = b.replace(d, c)
        }
    }
    return b
}
function dspace_makeFieldInput(a, c) {
    var b = a.search("_[0-9]+$");
    if (b < 0) {
        return a + c
    } else {
        return a.substr(0, b) + c + a.substr(b)
    }
}
function DSpaceUpdateConfidence(g, a, f) {
    if (a == null || a == "") {
        return
    }
    var e = g.getElementById(a);
    if (e == null) {
        return
    }
    if (e.className == null) {
        e.className = "cf-" + f
    } else {
        var c = e.className.split(" ");
        var h = "";
        var d = false;
        for (var b = 0; b < c.length; ++b) {
            if (c[b].match("^cf-[a-zA-Z0-9]+$")) {
                h += "cf-" + f + " ";
                d = true
            } else {
                h += c[b] + " "
            }
        }
        if (!d) {
            h += "cf-" + f + " "
        }
        e.className = h
    }
}
function DSpaceAuthorityOnChange(b, e, a) {
    var c = "accepted";
    if (e != null && e != "") {
        var d = document.getElementById(e);
        if (d != null) {
            d.value = c
        }
    }
    DSpaceUpdateConfidence(document, a, c);
    return false
}
function DSpaceToggleAuthorityLock(f, b) {
    if (b == null || b == "") {
        return false
    }
    var a = document.getElementById(b);
    if (a == null) {
        return false
    }
    var e = f.className.split(" ");
    var h = "";
    var c = false;
    var g = false;
    for (var d = 0; d < e.length; ++d) {
        if (e[d] == "is-locked") {
            c = false;
            g = true
        } else {
            if (e[d] == "is-unlocked") {
                c = true;
                g = true
            } else {
                h += e[d] + " "
            }
        }
    }
    if (!g) {
        return false
    }
    f.className = h + (c ? "is-locked" : "is-unlocked") + " ";
    a.readOnly = c;
    return false
}
;