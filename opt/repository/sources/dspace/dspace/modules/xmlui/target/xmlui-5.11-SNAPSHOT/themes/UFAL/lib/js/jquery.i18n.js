/*!
 * jQuery i18n plugin
 * @requires jQuery v1.1 or later
 *
 * See https://github.com/recurser/jquery-i18n
 *
 * Licensed under the MIT license.
 *
 * Version: 1.1.1 (Sun, 05 Jan 2014 05:26:50 GMT)
 */
(function(b){var c=Array.prototype.slice;var a={defaultLocale:"en",dicts:{en:{}},load:function(d,e){if(!d){d=this.defaultLocale}if(this.dicts[d]){b.extend(this.dicts[d],e)}else{this.dicts[d]=e}},_:function(d){currentLocale=b("#ds-language-selection").attr("data-locale");if(!currentLocale){currentLocale=this.defaultLocale}dict=this.dicts[currentLocale];if(dict&&dict.hasOwnProperty(d)){d=dict[d]}args=c.call(arguments);args[0]=d;return this.printf.apply(this,args)},printf:function(e,d){if(arguments.length<2){return e}d=b.isArray(d)?d:c.call(arguments,1);return e.replace(/([^%]|^)%(?:(\d+)\$)?s/g,function(h,g,f){if(f){return g+d[parseInt(f)-1]}return g+d.shift()}).replace(/%%s/g,"%s")}};b.fn._t=function(e,d){return b(this).html(a._.apply(a,arguments))};b.i18n=a})(jQuery);